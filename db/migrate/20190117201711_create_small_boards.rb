class CreateSmallBoards < ActiveRecord::Migration[5.2]
  def change
    create_table :small_boards do |t|
      t.integer :game_id
      t.integer :position
      t.string :winner, default: ''
      t.text :board

      t.timestamps
    end
  end
end
