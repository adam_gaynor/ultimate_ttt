class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :winner
      t.string :turn
      t.text :board

      t.timestamps
    end
  end
end
