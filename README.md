# Local Deployment
1. Make sure that Postgres and Redis are installed, as well as Node and npm.
2. `bundle install` and `rails db:create db:migrate`
3. In separate tabs, run:
  a. rails server
4. Navigate to "http://localhost:3000"
