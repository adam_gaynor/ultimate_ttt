module BoardHelper
  def check_winner(board_arr, move_cell)
    grid = Grid.new(board_arr)
    position_to_check = {row: move_cell / 3, col: move_cell % 3}
    grid.winning_move?(position_to_check[:row], position_to_check[:col])
  end
end
