class Grid
  BOARD_SIZE = 3

  attr_accessor :grid

  def initialize(arr)
    @grid ||= [arr[0..2], arr[3..5], arr[6..8]]
  end

  def [](*position)
    row, column = position
    grid[row][column]
  end

  def []=(*position, value)
    row, column = position
    grid[row][column] = value
  end

  def winning_move?(*position)
    row_idx, col_idx = position
    winning_row?(row_idx) || winning_column?(col_idx) || winning_diagonal?(row_idx, col_idx)
  end

  def winning_row?(row_idx)
    # The board is arranged into rows by default, so can be accessed with the index
    winning_combination?(grid[row_idx].uniq)
  end

  def winning_column?(col_idx)
    # Columns need to be derived from coordinates
    column_content = (0...BOARD_SIZE).inject([]) do |accum, row_idx|
      accum << self[row_idx, col_idx]
    end

    winning_combination?(column_content.uniq)
  end

  def winning_diagonal?(row_idx, col_idx)
    # If row and column index are the same, check the diagonal
    # where they are the same for all squares
    if row_idx == col_idx
      content = []
      (0...BOARD_SIZE).each { |idx| content << self[idx, idx] }
      return true if winning_combination?(content.uniq)
    end

    # If the coordinates of the square to check add up to 1 less than the length
    # of the board (because arrays start at 0), check all squares where that is the case
    combined_size = BOARD_SIZE - 1
    if row_idx + col_idx == combined_size
      content = []
      row = 0
      col = combined_size
      BOARD_SIZE.times do
        content << self[row, col]
        row += 1
        col -= 1
      end
      return true if winning_combination?(content.uniq)
    end

    false
  end

  def winning_combination?(content)
    # If the entire row/column/diagonal has only one value, and is not nil, then that player wins
    content.length == 1 && !content.first.nil?
  end
end
