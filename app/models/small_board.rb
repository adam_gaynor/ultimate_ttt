class SmallBoard < ApplicationRecord
  include BoardHelper
  serialize :board
  belongs_to :game

  def move(cell)
    board[cell] = game.turn
    self.save!
    game.switch_turn

    check_winner(board, cell)
  end

  def winner
    check_winner(self.board)
  end
end
