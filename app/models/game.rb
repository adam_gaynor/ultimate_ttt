class Game < ApplicationRecord
  serialize :board
  has_many :small_boards

  def create_board!
    (0..8).each do |n|
      SmallBoard.create!(game_id: self.id, position: n, board: ["","","","","","","","",""])
    end
  end

  def output
    small_boards.order(:position).map(&:board)
  end

  def move(small_board_pos: , cell: )
    small_board = small_boards.find_by(position: small_board_pos)
    small_board.move(cell)
  end

  def switch_turn
    next_turn = self.turn == "X" ? "O" : "X"
    self.update!(turn: next_turn)
  end
end
