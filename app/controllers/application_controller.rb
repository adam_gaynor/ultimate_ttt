class ApplicationController < ActionController::Base
  # This would normally be a BAD thing
  skip_before_action :verify_authenticity_token, raise: false
end
