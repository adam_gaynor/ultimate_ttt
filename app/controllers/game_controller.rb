class GameController < ApplicationController
  def show
    game = Game.find(params[:id])

    if game
      render json: {
        turn: game.turn,
        winner: game.winner,
        board: game.output
      }
    else
      render plain: "Invalid Id", status: 404
    end
  end

  def create
    game = Game.create!(turn: 'X', winner: '')
    game.create_board!

    render json: {
      id: game.id,
      turn: game.turn,
      winner: game.winner,
      board: game.output
    }
  end

  def move
    game = Game.find(params[:id])
    game.move(small_board_pos: params[:subgame].to_i, cell: params[:cell].to_i)

    render json: {
      id: game.id,
      turn: game.turn,
      winner: game.winner,
      board: game.output
    }
  end
end
