Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#home'


  get '/game/:id', to: 'game#show'
  post '/game', to: 'game#create'
  post '/move', to: 'game#move'
end
